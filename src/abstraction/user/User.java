/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstraction.user;

import abstraction.appdata.materias.Grupo;
import abstraction.appdata.materias.Profesor;
import abstraction.user.horario.Horario;
import java.util.ArrayList;

/**
 *
 * @author Jeremmy
 */
public class User {

    public ArrayList<Grupo> grupos;
    public Horario horario;
    public ArrayList<Profesor> profesores;
    

    public User() {
        horario = new Horario();
        grupos = new ArrayList<>();
        profesores = new ArrayList<>();
    }

    public Grupo grupo_existe(String g) {
        for (Grupo grupo : grupos) {
            if (grupo.name.equalsIgnoreCase(g)) {
                return grupo;
            }
        }
        return null;
    }
    
     public Profesor profe_existe(String p) {
        for (Profesor pr : profesores) {
            if (pr.name.equalsIgnoreCase(p)) {
                return pr;
            }
        }
        return null;
    }
}
