/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstraction.user.horario;

import abstraction.appdata.materias.Bloque;
import abstraction.appdata.materias.Materia;
import java.util.ArrayList;

/**
 *
 * @author Jeremmy
 */
public class Dia {
    int dia;
    public ArrayList<Bloque> bloques;
    
    public Dia(){
        bloques = new ArrayList<>();
    }
    
    public void addBloque(String hora_inicio, String hora_fin, Materia materia,String edificio, String seccion){
        this.bloques.add(new Bloque(hora_inicio, hora_fin, edificio, seccion, dia, materia));
    }
    
    public void addBloque(String hora_inicio, String hora_fin, Materia materia){
        this.bloques.add(new Bloque(hora_inicio, hora_fin, "", "", dia, materia));
    }
    
    public Bloque bloque_existe(String hora_inicio, String hora_fin){
        for (Bloque bloque : bloques) {
            if(bloque.dia == dia){
                if(bloque.hora_inicio.equals(hora_inicio)){
                    if(bloque.hora_fin.equals(hora_fin)){
                        return bloque;
                    }
                }
            }
        }
        
        return null;
    }
}
