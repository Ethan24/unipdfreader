/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstraction.user.horario;

/**
 *
 * @author Jeremmy
 */
public class Horario {
    public Dia Lunes;
    public Dia Martes;
    public Dia Miercoles;
    public Dia Jueves;
    public Dia Viernes;
    
    public Horario(){
        int i = 0;
        Lunes = new Dia();
        Martes = new Dia();
        Miercoles = new Dia();
        Jueves = new Dia();
        Viernes = new Dia();
         
        Lunes.dia = i++;
        Martes.dia = i++;
        Miercoles.dia = i++;
        Jueves.dia = i++;
        Viernes.dia = i++;
    }
    
    public Dia dia(int n){
        switch (n) {
            case 0:
                return Lunes;
            case 1:
                return Martes;
            case 2:
                return Miercoles;
            case 3:
                return Jueves;
            case 4:
                return Viernes;
        }
        return null;
    }
}
