package abstraction.appdata.materias;


import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Jeremmy
 */
public class Grupo {
    public String name;
    public ArrayList<Materia> materias;

    public Grupo(String name) {
        this.name = name;
        materias = new ArrayList<>();
    }
    
     public Materia clase_existe(String t) {
        for (Materia m : materias) {
            if (m.name.equalsIgnoreCase(t)) {
                return m;
            }
        }
        return null;
    }
}
