package abstraction.appdata.materias;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Jeremmy
 */
public class Bloque {
    public Materia materia;
    public String hora_inicio;
    public String hora_fin;
    public String edificio;
    public String seccion;
    public int dia;

    public Bloque(String hora_inicio, String hora_fin, String lugar, String seccion, int dia, Materia materia) {
        this.hora_inicio = hora_inicio;
        this.hora_fin = hora_fin;
        this.edificio = lugar;
        this.seccion = seccion;
        this.dia = dia;
        this.materia = materia;
    }    
    
    
    public Bloque(String hora_inicio, String hora_fin, int dia, Materia materia) {
        this.hora_inicio = hora_inicio;
        this.hora_fin = hora_fin;
        this.dia = dia;
        this.materia = materia;
        edificio = "";
        seccion = "";
    }  
    
}
