package abstraction.appdata.materias;


import java.awt.Color;
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Jeremmy
 */
public class Materia {
    public String name;
    Color color;
    String Semestre;
    public String grupo;
    public Profesor profesor;
    public ArrayList<Bloque> bloques;
    
    public Materia(String name, Profesor p, String g){
        this.name = name;
        int v = 0;
        profesor = p;
        bloques = new ArrayList<>();
        grupo = g;
    }
    
    public void addBloque(String hora_inicio, String hora_fin, String edificio, String seccion, int dia){
        this.bloques.add(new Bloque(hora_inicio, hora_fin, edificio, seccion, dia, this));
    }
    
    public Bloque bloque_existe(String hora_inicio, String hora_fin, int dia){
        for (Bloque bloque : bloques) {
            if(bloque.dia == dia){
                if(bloque.hora_inicio.equals(hora_inicio)){
                    if(bloque.hora_fin.equals(hora_fin)){
                        return bloque;
                    }
                }
            }
        }
        
        return null;
    }
   
}
