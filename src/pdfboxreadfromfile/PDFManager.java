/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pdfboxreadfromfile;

import abstraction.appdata.materias.Bloque;
import abstraction.appdata.materias.Grupo;
import abstraction.appdata.materias.Materia;
import abstraction.appdata.materias.Profesor;
import abstraction.user.User;
import abstraction.user.horario.Dia;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.PDFTextStripperByArea;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.lang.Exception;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PDFManager {

    ArrayList<Materia> clases;

    float MM_TO_UNITS = 1 / (10 * 2.54f) * 72;

    Periodo m1 = new Periodo();
    Periodo m2 = new Periodo();
    Periodo m3 = new Periodo();

    int getDocumentPageCount;
    private PDDocument document;
    User user;
    TDia Lunes;
    TDia Martes;
    TDia Miercoles;
    TDia Jueves;
    TDia Viernes;

    public PDFManager(String filepath, User user) {
        this.user = user;

        clases = new ArrayList<>();

        Lunes = new TDia(user.horario.Lunes, 25, 40.6, 0);
        Martes = new TDia(user.horario.Martes, 60, 40.6, 1);
        Miercoles = new TDia(user.horario.Miercoles, 100, 40.6, 2);
        Jueves = new TDia(user.horario.Jueves, 140, 35.6, 3);
        Viernes = new TDia(user.horario.Lunes, 175, 45, 4);

        document = null;
        if (filepath.endsWith(".pdf")) {
            File file = new File(filepath);

            try {
//                PDFParser parser = new PDFParser(new RandomAccessFile(file, "r")); // update for PDFBox V 2.0
//                parser.parse();

//                COSDocument cosDoc = parser.getDocument();
                document = PDDocument.load(file);
//System.out.println("puto1");
//                document = new PDDocument(cosDoc);
                getDocumentPageCount = document.getNumberOfPages();
//                System.out.println(getDocumentPageCount);
            } catch (Exception ex) {
                Logger.getLogger(PDFManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            System.out.println("No data return");
        }
    }

    public PDFManager() {

    }

    /**
     *
     * @return @throws Exception
     */
    private char[] tolower(char[] uwu) {
        for (int i = 0; i < uwu.length; i++) {
            uwu[i] = Character.toLowerCase(uwu[i]);
        }

        return uwu;
    }

    private String trate_hora(String txt) {
        if (!txt.contains("p.m") && txt.contains("a.m")) {
            txt = txt.substring(0, txt.indexOf("a.m"));
            txt += "am";
        } else if (txt.contains("p.m") && !txt.contains("a.m")) {
            txt = txt.substring(0, txt.indexOf("p.m"));
            txt += "pm";
        }

        return txt;
    }

    private String findHora(Point.Double rp, double width, double time_period_height, double line_height, int page) {
        String txt = "";
        Rectangle2D hora;
        while (true) {
            hora = new Rectangle2D.Double(rp.x * MM_TO_UNITS, rp.y * MM_TO_UNITS, width * MM_TO_UNITS, time_period_height * MM_TO_UNITS);
            try {
                txt = getTextUsingCoordinates(page, hora);
            } catch (Exception ex) {
                Logger.getLogger(PDFBoxReadFromFile.class.getName()).log(Level.SEVERE, null, ex);
            }
            txt = txt.trim();

            if (txt.length() > 11) {
                break;
            }

            if (rp.y >= 151) {
                return null;
            }

            rp.y += line_height;
        }

        return txt;
    }

    private void DEf_Horas(int page) {
        String text = "uwu";

        double line_height = 4;
        double time_period_height = 4.3 * 2;
        double minim_height = line_height * 5;
        Point.Double rp = new Point.Double(10, 52.51);//54.51
        double width = 10;

        double total_Readable = 146.49;

        m1.x = rp.x;
        m1.y = rp.y;

        Rectangle2D hora = new Rectangle2D.Double(rp.x * MM_TO_UNITS, rp.y * MM_TO_UNITS, width * MM_TO_UNITS, time_period_height * MM_TO_UNITS);

        try {
            text = getTextUsingCoordinates(page, hora);
        } catch (Exception ex) {
            Logger.getLogger(PDFBoxReadFromFile.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(text + " (" + m1.x + ", " + m1.y + ")");

        m1.inicio = trate_hora(text.substring(0, text.indexOf('\n')));
        m1.fin = trate_hora(text.substring(text.indexOf('\n') + 1));

        rp.y += minim_height;

        text = findHora(rp, width, time_period_height, line_height, page);

        if (text == null) {
            m2.inicio = null;
            m3.inicio = null;
        } else {
            m2.x = rp.x;
            m2.y = rp.y;
            System.out.println(text + " (" + m2.x + ", " + m2.y + ")");
            //System.out.println(rp.y);
            rp.y += minim_height;

            m2.inicio = trate_hora(text.substring(0, text.indexOf('\n')));
            m2.fin = trate_hora(text.substring(text.indexOf('\n') + 1));

            text = findHora(rp, width, time_period_height, line_height, page);
            if (text == null) {
                m3.inicio = null;
            } else {
                m3.x = rp.x;
                m3.y = rp.y;
                System.out.println(text + " (" + m3.x + ", " + m3.y + ")");

                rp.y += minim_height;
                m3.inicio = trate_hora(text.substring(0, text.indexOf('\n')));
                m3.fin = trate_hora(text.substring(text.indexOf('\n') + 1));
            }
        }

        if (m2.inicio == null) {
            m1.height = total_Readable - m1.y;
        }else{
            m1.height = m2.y - m1.y;
            if(m3.inicio == null){
                m2.height = total_Readable - m2.y;
            }else{
                m2.height = m3.y - m2.y;
                m3.height = total_Readable - m2.height - m1.height;
                System.out.println("puto");
            }
        }

//        ftext = "Primer: " + m1.inicio + " / " + m1.fin
//                + "\nSegundo: " + m2.inicio + " / " + m2.fin
//                + "\nTercero: " + m3.inicio + " / " + m3.fin;
    }

    List<Integer> extraerNumeros(String cadena) {
        List<Integer> todosLosNumeros = new ArrayList<>();
        Matcher encuentrador = Pattern.compile("\\d+").matcher(cadena);
        while (encuentrador.find()) {
            todosLosNumeros.add(Integer.parseInt(encuentrador.group()));
        }
        return todosLosNumeros;
    }

    private String clasFormat(String t) {
        if (t.contains(" De ")) {
            t = t.replace(" De ", " de ");
        }

        if (t.contains(" La ")) {
            t = t.replace(" La ", " la ");
        }

        if (t.contains(" A ")) {
            t = t.replace(" A ", " a ");
        }

        List<Integer> n = extraerNumeros(t);

        if (n.size() > 0) {
            t = t.substring(0, t.indexOf(n.get(0).toString()));
            t += Numeros2Romanos(n.get(0));
        }
//        System.out.println(t);

        return t;
    }

    private String nojumps(String t) {
        return t.replace("\n", "").replace("\r", "").trim();
    }

    private ArrayList<String> getClassData(String txt) {
        ArrayList<String> data = new ArrayList<>();
        String materia;
        String grupo;
        String edificio;
        String aula;
        String docente;

        materia = clasFormat(convierte_formato(txt.substring(0, txt.indexOf(",")))).trim();

        grupo = nojumps(txt.substring(txt.indexOf("Grupo: ") + "Grupo: ".length(), txt.indexOf("Aula")).trim());

        edificio = convierte_formato(txt.substring(txt.indexOf("Edificio: ") + "Edificio: ".length(), txt.indexOf("Docente")));
        if (!edificio.contains("Edificio")) {
            edificio = "Edificio " + edificio;
        }

        aula = nojumps(txt.substring(txt.indexOf("Aula: ") + "Aula: ".length(), txt.indexOf("Edificio")));

        docente = convierte_formato(txt.substring(txt.indexOf("Docente: ") + "Docente: ".length()));


        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println(docente);
        data.add(materia);
        data.add(grupo);
        data.add(aula);
        data.add(edificio);
        data.add(docente);

        //System.out.println(data);
        return data;
    }

    public static String Numeros2Romanos(int numero) {
        int i, decenas, unidades;
        StringBuilder romano = new StringBuilder();

        //obtenemos cada cifra del número
        decenas = numero / 10 % 10;
        unidades = numero % 10;

        //decenas
        if (decenas == 9) {
            romano.append("XC");
        } else if (decenas >= 5) {
            romano.append("L");
            for (i = 6; i <= decenas; i++) {
                romano.append("X");
            }
        } else if (decenas == 4) {
            romano.append("XL");
        } else {
            for (i = 1; i <= decenas; i++) {
                romano.append("X");
            }
        }

        //unidades
        if (unidades == 9) {
            romano.append("IX");
        } else if (unidades >= 5) {
            romano.append("V");
            for (i = 6; i <= unidades; i++) {
                romano.append("I");
            }
        } else if (unidades == 4) {
            romano.append("IV");
        } else {
            for (i = 1; i <= unidades; i++) {
                romano.append("I");
            }
        }
        return romano.toString();
    }

    public String convierte_formato(String string) {
        if (string == null) {
            return null;
        }
        string = nojumps(string).trim();

        String romanos = "";

        if (string.contains(" I") && string.indexOf(" I") + " I".length() == string.length()) {
            string = string.replace(" I", "");
            romanos = "I";
        }

        if (string.contains(" II") && string.indexOf(" II") + " II".length() == string.length()) {
            string = string.replace(" II", "");
            romanos = "II";
        }

        if (string.contains(" III") && string.indexOf(" III") + " III".length() == string.length()) {
            string = string.replace(" III", "");
            romanos = "III";
        }

        if (string.contains(" IV") && string.indexOf(" IV") + " IV".length() == string.length()) {
            string = string.replace(" IV", "");
            romanos = "IV";
        }

        if (string.contains(" V") && string.indexOf(" V") + " V".length() == string.length()) {
            string = string.replace(" V", "");
            romanos = "V";
        }

        if (string.contains(" VI") && string.indexOf(" VI") + " VI".length() == string.length()) {
            string = string.replace(" VI", "");
            romanos = "VI";
        }

        if (string.contains(" VII") && string.indexOf(" VII") + " VII".length() == string.length()) {
            string = string.replace(" VII", "");
            romanos = "VII";
        }

        StringBuilder builder = new StringBuilder();
        StringTokenizer st = new StringTokenizer(string, " ");
        while (st.hasMoreElements()) {
            String ne = (String) st.nextElement();
            if (ne.length() > 0) {
                builder.append(ne.substring(0, 1).toUpperCase());
                builder.append(ne.substring(1).toLowerCase()); //agregado
                builder.append(' ');
            }
        }

        return builder.toString().concat(romanos).trim();
    }

    private void getDay_box(TDia dia, Periodo p, int page) {
        ArrayList<String> data;
        Rectangle2D section = new Rectangle2D.Double(dia.x * MM_TO_UNITS, p.y * MM_TO_UNITS, dia.width * MM_TO_UNITS, p.height * MM_TO_UNITS);
        String text = " ";

        Bloque bloque;
        Grupo grupo;
        Profesor profesor;
        Materia materia;

        try {
            text = getTextUsingCoordinates(page, section);
        } catch (Exception ex) {
            Logger.getLogger(PDFBoxReadFromFile.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (nojumps(text.trim()).replace(" ", "").length() <= 2) {
            bloque = new Bloque(p.inicio, p.fin, dia.n, null);
        } else {
            System.out.println(dia.n);
            System.out.println(p.inicio+", "+p.fin);
            System.out.println(page);
            System.out.println(text);
            data = getClassData(text);
            //data 0    materia
            //data 1    grupo
            //data 2    aula
            //data 3    edificio
            //data 4    docente

            grupo = user.grupo_existe(data.get(1));

            if (grupo == null) {
                grupo = new Grupo(data.get(1));
                user.grupos.add(grupo);
            }

            profesor = user.profe_existe(data.get(4));
//            System.out.println("--------------------\n\n"+profesor.name+"\n\n");

            if (profesor == null) {
                profesor = new Profesor(data.get(4));
                user.profesores.add(profesor);
            }

            materia = grupo.clase_existe(data.get(0));
            if (materia == null) {
                materia = new Materia(data.get(0), profesor, grupo.name);
                grupo.materias.add(materia);
            }

            bloque = new Bloque(p.inicio, p.fin, data.get(3), data.get(2), dia.n, materia);
            materia.bloques.add(bloque);
        }

        switch (dia.n) {
            case 0:
                user.horario.Lunes.bloques.add(bloque);
                break;
            case 1:
                user.horario.Martes.bloques.add(bloque);
                break;
            case 2:
                user.horario.Miercoles.bloques.add(bloque);
                break;
            case 3:
                user.horario.Jueves.bloques.add(bloque);
                break;
            case 4:
                user.horario.Viernes.bloques.add(bloque);
                break;
        }

//        System.out.println("Dia" + bloque.dia);
//        System.out.println("Bloque " + user.horario.dia(dia.n).bloque_existe(p.inicio, p.fin).hora_inicio + " / " + user.horario.dia(dia.n).bloque_existe(p.inicio, p.fin).hora_fin);
//        if (materia != null) {
//            System.out.println("Materia " + user.horario.dia(dia.n).bloque_existe(p.inicio, p.fin).materia.name);
//            System.out.println("Grupo " + user.horario.dia(dia.n).bloque_existe(p.inicio, p.fin).materia.grupo);
//            System.out.println(user.horario.dia(dia.n).bloque_existe(p.inicio, p.fin).edificio);
//            System.out.println("Seccion " + user.horario.dia(dia.n).bloque_existe(p.inicio, p.fin).seccion);
//            System.out.println("Profesor " + user.horario.dia(dia.n).bloque_existe(p.inicio, p.fin).materia.profesor.name);
//        } else {
//            System.out.println("empty");
//        }
    }

    private void day_read(TDia d, int page) {
        if (m1.inicio != null) getDay_box(d, m1, page);
        if (m2.inicio != null) getDay_box(d, m2, page);
        if (m3.inicio != null) getDay_box(d, m3, page);
    }

    public void read_horario() {
        if (getDocumentPageCount > 0) {
            int page = 1;

            while (page <= getDocumentPageCount) {
                DEf_Horas(page);
                day_read(Lunes, page);
                day_read(Martes, page);
                day_read(Miercoles, page);
                day_read(Jueves, page);
                day_read(Viernes, page);

                page++;
            }
        }
    }

    public String Student_Name() {
        Rectangle2D section = new Rectangle2D.Double(30 * MM_TO_UNITS, 40 * MM_TO_UNITS, 150 * MM_TO_UNITS, 1 * MM_TO_UNITS);
        String text = "uwu";

        try {
            text = getTextUsingCoordinates(1, section);
        } catch (Exception ex) {
            Logger.getLogger(PDFBoxReadFromFile.class.getName()).log(Level.SEVERE, null, ex);
        }

        char[] name = tolower(text.toCharArray());

        name[0] = Character.toUpperCase(name[0]);

        while (true) {
            int i = text.indexOf(' ');

            if (i < 1 || i >= name.length + 1) {
                break;
            }
            text = text.substring(0, i) + "-" + text.substring(i + 1);

            name[i + 1] = Character.toUpperCase(name[i + 1]);
        }

        text = String.copyValueOf(name);

        read_horario();

        return text;
    }

    public void close() {
        try {
            document.close();
        } catch (Exception ex) {
            Logger.getLogger(PDFManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String getall(String f) {
        String text = "uwu";
        try {
            PDDocument doc = PDDocument.load(new File(f));
            text = new PDFTextStripper().getText(doc);
        } catch (Exception ex) {
            Logger.getLogger(PDFManager.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ex.getMessage());
        }
        return text;
    }

    public String getTextUsingCoordinates(int pageNumber, Rectangle2D rect) {
        String extractedText = "";
        //System.out.println(rect.getY() / MM_TO_UNITS);
        // PDDocument Creates an empty PDF document. You need to add at least
        // one page for the document to be valid.
        // Using load method we can load a PDF document
        PDPage page = new PDPage();
        try {
            // Get specific page. THe parameter is pageindex which starts with // 0. If we need to
            // access the first page then // the pageIdex is 0 PDPage
            if (getDocumentPageCount > 1) {
                page = document.getPage(--pageNumber);
            } else if (getDocumentPageCount >= 0) {
                page = document.getPage(0);
            }
            // To create a rectangle by passing the x axis, y axis, width and height 
            String regionName = "region1";

            // Strip the text from PDF using PDFTextStripper Area with the
            // help of Rectangle and named need to given for the rectangle
            PDFTextStripperByArea stripper = new PDFTextStripperByArea();
            stripper.setSortByPosition(false);
            stripper.addRegion(regionName, rect);
            stripper.extractRegions(page);
            //System.out.println("Region is " + stripper.getTextForRegion("region1"));
            extractedText = stripper.getTextForRegion("region1");

        } catch (Exception e) {
            System.out.println("The file  not found" + "");
        }
        // Return the extracted text and this can be used for assertion
        return extractedText;
    }

    boolean isAvalidUNItimeTable() {
        String title = "";
        String subtitle = "";
        Rectangle2D t = new Rectangle2D.Double(92 * MM_TO_UNITS, 16 * MM_TO_UNITS, 110 * MM_TO_UNITS, 5 * MM_TO_UNITS);
        Rectangle2D subt = new Rectangle2D.Double(120 * MM_TO_UNITS, 27 * MM_TO_UNITS, 50 * MM_TO_UNITS, 5 * MM_TO_UNITS);

        try {
            title = nojumps(getTextUsingCoordinates(1, t));
        } catch (Exception ex) {
            Logger.getLogger(PDFBoxReadFromFile.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            subtitle = nojumps(getTextUsingCoordinates(1, subt));
        } catch (Exception ex) {
            Logger.getLogger(PDFBoxReadFromFile.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (title.equals("UNIVERSIDAD NACIONAL DE INGENIERÍA")) {
            return subtitle.equals("H O R A R I O");
        }
        return false;
    }
}

class TDia {

    Dia dia;
    int n;
    double x;
    double width;

    public TDia(Dia dia, double x, double width, int number) {
        this.dia = dia;
        this.x = x;
        this.width = width;
        n = number;
    }
}

class Periodo {

    Periodo() {
        x = -123;
        y = -123;
    }

    String inicio;
    String fin;
    double x;
    double y;
    double height;
}
